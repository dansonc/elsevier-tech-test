package com.mendeley.coding;

import java.util.List;

public interface Checkout {

    String checkout(List<Item> items);
}
